﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Resources;
using System.Web.Http;

namespace AngularTranslateResx.Controllers
{
  [RoutePrefix("api/i18n")]
  public class TranslationController : ApiController
  {
    [Route("{language}")]
    public JObject Get(string language)
    {
      //dynamic translations = this.GetStaticTranslations(language);

      dynamic translations = this.GetTranslationsFromResx(language);

      return translations;
    }

    //private JObject GetStaticTranslations(string language)
    //{
    //  dynamic translations = new JObject();

    //  if (language.Equals("de", StringComparison.OrdinalIgnoreCase))
    //  {
    //    translations.HEADLINE = "Was für ein großartiges Modul!";
    //    translations.PARAGRAPH = "Ernsthaft!";
    //    translations.PASSED_AS_TEXT = "Hey! Ich wurde  als text übergeben!";
    //    translations.PASSED_AS_ATTRIBUTE = "Ich wurde als Attribut übergeben cool oder?";
    //    translations.PASSED_AS_INTERPOLATION = "Anfänger! Ich bin interpoliert!";
    //    translations.VARIABLE_REPLACEMENT = "Hi {{name}}";
    //    translations.BUTTON_LANG_DE = "Deutsch";
    //    translations.BUTTON_LANG_EN = "Englisch";
    //  }
    //  else
    //  {
    //    translations.HEADLINE = "What an awesome module!";
    //    translations.PARAGRAPH = "Srsly!";
    //    translations.PASSED_AS_TEXT = "Hey there! I'm passed as text value!";
    //    translations.PASSED_AS_ATTRIBUTE = "I'm passed as attribute value cool ha?";
    //    translations.PASSED_AS_INTERPOLATION = "Beginners! I'm interpolated!";
    //    translations.VARIABLE_REPLACEMENT = "Hi {{name}}";
    //    translations.BUTTON_LANG_DE = "German";
    //    translations.BUTTON_LANG_EN = "English";
    //  }

    //  return translations;
    //}

    private JObject GetTranslationsFromResx(string language)
    {
      JObject translations = new JObject();

      var assembly = Assembly.GetExecutingAssembly();
      Type resourceType = assembly.GetType("AngularTranslateResx.Resources." + language);
      PropertyInfo[] resourceProps = resourceType.GetProperties(
          BindingFlags.NonPublic |
          BindingFlags.Static |
          BindingFlags.GetProperty);

      foreach (PropertyInfo info in resourceProps)
      {
        string name = info.Name;
        object value = info.GetValue(null, null);

        if (value is string && info.CustomAttributes.Count() == 0)
        {
          translations.Add(name, value.ToString());
        }
      }

      return translations;
    }
  }
}
