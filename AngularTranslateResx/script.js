﻿var app = angular.module('myApp', ['ngCookies', 'pascalprecht.translate']);

app.config(['$translateProvider', function ($translateProvider) {
    $translateProvider.preferredLanguage('en');
    $translateProvider.useLoader('asyncLoader');
}]);

app.factory('asyncLoader', ['$http', '$q', '$timeout', function ($http, $q, $timeout) {
    return function (options) {
        /*
        /////////////////////////////////////////
        // Static translations in client.

        var deferred = $q.defer();
        var translations;

        if (options.key === 'en') {
            translations = {
                HEADLINE: 'What an awesome module!',
                PARAGRAPH: 'Srsly!',
                PASSED_AS_TEXT: 'Hey there! I\'m passed as text value!',
                PASSED_AS_ATTRIBUTE: 'I\'m passed as attribute value, cool ha?',
                PASSED_AS_INTERPOLATION: 'Beginners! I\'m interpolated!',
                VARIABLE_REPLACEMENT: 'Hi {{name}}',
                BUTTON_LANG_DE: 'German',
                BUTTON_LANG_EN: 'English'
            };
        } else {
            translations = {
                HEADLINE: 'Was für ein großartiges Modul!',
                PARAGRAPH: 'Ernsthaft!',
                PASSED_AS_TEXT: 'Hey! Ich wurde als text übergeben!',
                PASSED_AS_ATTRIBUTE: 'Ich wurde als Attribut übergeben, cool oder?',
                PASSED_AS_INTERPOLATION: 'Anfänger! Ich bin interpoliert!',
                VARIABLE_REPLACEMENT: 'Hi {{name}}',
                BUTTON_LANG_DE: 'Deutsch',
                BUTTON_LANG_EN: 'Englisch'
            };
        }

        $timeout(function () {
            deferred.resolve(translations);
        }, 2000);

        return deferred.promise;
        */

        /*
        /////////////////////////////////////////
        // Static json file translations.

        var deferred = $q.defer();
        var baseUrl = ''; //'app/i18n/'
        var language = options.key;

        $http.get(baseUrl + language + '.json')
                .then(function (result) {
                    return deferred.resolve(result.data);
                },
                function (err) {
                    return deferred.reject('');
                });

        return deferred.promise;
        */

        /////////////////////////////////////////
        // Resx translations from WebApi.

        var deferred = $q.defer();
        var baseUrl = 'api/i18n/';
        var language = options.key;

        $http.get(baseUrl + language)
                .then(function (result) {
                    return deferred.resolve(result.data);
                },
                function (err) {
                    return deferred.reject('');
                });

        return deferred.promise;
    };
}]);

app.controller('Ctrl', ['$translate', '$scope', function ($translate, $scope) {
    $scope.changeLanguage = function (langKey) {
        $translate.use(langKey);
    };
}]);